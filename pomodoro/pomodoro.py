#!/usr/bin/env python3
"""Simple command line Pomodoro app."""

# Icons
# - Pomodoro done: 
# - Pomodoro ticking: 
# - Pomodoro squashed: 
# - Pomodoro estimated: 


from __future__ import annotations

import argparse
import os
import sys
import time
from collections.abc import Callable
from datetime import datetime
from pathlib import Path
from subprocess import DEVNULL, CalledProcessError, check_call, check_output

display: Callable = print
script_dir = Path(__file__).parent.absolute()
ALARM_FILE_DIRS: list[Path] = [
    Path("."),
    script_dir,
    Path(sys.prefix),
    Path(script_dir, "..", ".."),
]
ALARM_FILENAME: str = "complete.oga"
ALARM_FILE: Path | None = None
for alarm_file_dir in ALARM_FILE_DIRS:
    ALARM_FILE = alarm_file_dir / ALARM_FILENAME
    if ALARM_FILE.is_file():
        break
ALARM_CMD_FFPLAY: list[str] = ["ffplay", "-nodisp", "-autoexit"]
ALARM_CMD_MPG123: list[str] = ["mpg123"]
ALARM_CMDS: tuple[list[str], ...] = (ALARM_CMD_FFPLAY, ALARM_CMD_MPG123)
DATA_FILENAME: Path = Path("~/.pomodoro").expanduser()
LIVE_FILENAME: Path = Path("~/.pomodoro_live").expanduser()


def notify_send(
    title: str,
    content: str,
    urgency: str = "normal",
    expire_time: int | None = None,
    hint: str | None = None,
):
    """Show a message via inotify.

    Args:
        title: The title for the notification.
        content: The content of the notification.
        urgency: This can be 'low', 'normal' or 'critical'.
        expire_time: The time in milliseconds to expire the message.
        hint: pass extra data. The form should be 'TYPE:NAME:VALUE'.
    """
    if urgency not in ("low", "normal", "critical"):
        urgency = "normal"
    notify_send_args = ["notify-send", f"--urgency={urgency}"]
    if expire_time is not None:
        notify_send_args.append(f"--expire-time={expire_time}")
    if hint is not None:
        notify_send_args.append(f"--hint={hint}")
    notify_send_args.extend([title, content])
    try:
        check_call(
            notify_send_args,
            stdout=DEVNULL,
            stderr=DEVNULL,
        )
    except CalledProcessError:
        display("Failed to run `notify-send`.")


def tick(duration: int, timer: bool, polybar: bool, pomodoro_type: str) -> bool:
    """Let the time pass for duration seconds.

    This can be interrupted by the user.

    Args:
        duration: time to pass in seconds.
        timer: whether or not to display the passing time.

    Returns:
        True if the timer was interrupted, False otherwise.
    """
    try:
        if timer:
            cli_timer(duration, polybar, pomodoro_type)
        else:
            cli_timer(duration, polybar, pomodoro_type, no_output=True)
    except KeyboardInterrupt:
        display("Interrupting")
        interrupt = True
    else:
        interrupt = False
    return interrupt


def play_alarm(filename: Path | None):
    """Play an (alarm) sound."""
    if filename is None or not filename.is_file():
        display(f"Soundfile not found: {filename}")
        return
    for alarm_cmd in ALARM_CMDS:
        play_cmd = alarm_cmd + [str(filename)]
        try:
            check_call(play_cmd, stdout=DEVNULL, stderr=DEVNULL)
        except (FileNotFoundError, CalledProcessError):
            # error, try the next alarm cmd
            continue
        else:
            # successful
            return


def write_pomo(start: datetime, stop: datetime, tag: str):
    """Write the data of a successful pomodoro to disk."""
    duration = (stop - start).total_seconds() / 60
    line = f"{tag},{start},{stop},{duration}\n"
    with DATA_FILENAME.open("a+") as datafile:
        datafile.write(line)


def trigger_polybar():
    """Trigger a polybar update."""
    # check_call(['echo', 'hook:module/pomodoro1', '>>', '/tmp/ipc-topbar'], shell=True)
    with open("/tmp/ipc-topbar", "w") as ipctopbar:
        ipctopbar.write("hook:module/pomodoro1")


def cli_timer(
    duration: int, polybar: bool, pomodoro_type: str, no_output: bool = False
):
    """Show the passing time on the command line and/or polybar."""
    for remaining in range(duration, -1, -1):
        # Stop the clock if the user is idle for more then 10 seconds.
        if pomodoro_type == "work":
            while is_user_idle(threshold=10000):
                time.sleep(1)
        # Stop the clock when the user is active during the break.
        # FIXME: silly me, this doesn't work of course... have to rethink this...
        # if pomodoro_type == "rest":
        #     while not is_user_idle(threshold=5000):
        #         time.sleep(5)
        hours, rem = divmod(remaining, 3600)
        mins, secs = divmod(rem, 60)
        fmt_time = f"{hours:0>2}:{mins:0>2}:{secs:0>2}"
        if not no_output:
            sys.stdout.write("\r")
            sys.stdout.write(f"Time remaining: {fmt_time}")
            sys.stdout.flush()
        if polybar:
            if pomodoro_type == "rest":
                icon = ""
            else:
                icon = ""
            with LIVE_FILENAME.open("w") as pomodorolive:
                pomodorolive.write(f"{icon}   {fmt_time}")
            trigger_polybar()
        time.sleep(1)
    display()


def is_user_idle(threshold: int = 1000) -> bool:
    """Detect if the user is idle.

    Args:
        threshold: time in miliseconds to consider the user really idle.
            If the time reported by `xssstate` is greater then the threshold,
            the user is considered idle.

    Returns:
        True if the user is idle, False otherwise.

    Note:
        This function relies on the availability of `xssstate` command line
        tool. If this is not found, the user will be considered active.
    """
    idletime: int = 0
    try:
        idletime = int(check_output(["xssstate", "-i"]))
    except (FileNotFoundError, ValueError):
        display("`xssstate` not available. The user is considered active!")
        return False
    return idletime > threshold


def cleanup_polybar():
    """Make sure the LIVE_FILENAME is removed and update polybar."""
    if LIVE_FILENAME.exists():
        os.remove(LIVE_FILENAME)
    trigger_polybar()


def start_pomodoro(
    work: int = 25,
    rest: int = 5,
    repeat: int = 10,
    alarm: bool = True,
    notify: bool = False,
    timer: bool = False,
    polybar: bool = False,
):
    """Start the pomodoro timer.

    Args:
        work: number of minuntes of work
        rest: number of minutes of rest
        repeat: number of cycles work-rest to do
        alarm: whether to play an alarm each time a pomodoro is finished or started
        notify: whether to send a message box each time a pomodoro is finished or
                started
        timer: whether to have cli timer visible
        polybar: if True: output status to LIVE_FILENAME and trigger polybar hook
    """
    for _ in range(repeat):
        display("Work now")
        if notify:
            # notify_send(' pomodoro', 'Work now.')
            notify_send("🍅 pomodoro", "Work now.")
        if alarm:
            play_alarm(ALARM_FILE)
        start = datetime.now()
        interrupted = tick(work * 60, timer, polybar, "work")
        if interrupted:
            if polybar:
                cleanup_polybar()
            break
        stop = datetime.now()
        write_pomo(start, stop, "work")
        display("Rest now")
        if alarm:
            play_alarm(ALARM_FILE)
        if notify:
            notify_send(
                # ' pomodoro',
                "😴 pomodoro",
                "Finished pomodoro, rest now.",
                # expire_time=rest * 1000 * 60,
                # hint="string:bgcolor:#773333",
            )
        start = datetime.now()
        interrupted = tick(rest * 60, timer, polybar, "rest")
        if interrupted:
            if polybar:
                cleanup_polybar()
            break
        stop = datetime.now()
        write_pomo(start, stop, "rest")

        display("Cycle complete")
        # Don't start next cycle when the user is idle
        while is_user_idle():
            time.sleep(1.0)


def main() -> None:
    """Start a Pomodoro timer."""
    parser = argparse.ArgumentParser()
    parser.add_argument("--work", default=25, type=int)
    parser.add_argument("--rest", default=5, type=int)
    parser.add_argument("--no-alarm", action="store_true")
    parser.add_argument("--no-notify", action="store_true")
    parser.add_argument("--repeat", default=10, type=int)
    parser.add_argument("--no-timer", action="store_true")
    parser.add_argument("--no-polybar", action="store_true")
    args = parser.parse_args()
    alarm = not args.no_alarm
    notify = not args.no_notify
    timer = not args.no_timer
    polybar = not args.no_polybar
    start_pomodoro(
        args.work,
        args.rest,
        args.repeat,
        alarm,
        notify,
        timer,
        polybar,
    )


if __name__ == "__main__":
    main()
